---
output: html_document
editor_options: 
  chunk_output_type: inline
---
# Install

For testing, R version 3.5.3 was used, configured with the flags ./configure --with-tcltk --enable-R-shlib

```
install.packages("devtools")
library(devtools)
install_bitbucket("cesaremov/targetdecoy_mining", build_vignettes = TRUE)
```

See the Target-DecoyMineR vignette:

```
utils::browseVignettes("TargetDecoyMineR")
```


## Requirement FFTW
If the installation fails with the error
```
fftwtools.c:28:9: fatal error: fftw3.h: No such file or directory
```
you need the FFTW <http://www.fftw.org/>


### Conda (<https://conda.io>, crossplatform) 
```
conda install -c menpo fftw
conda install -c eumetsat fftw3
```

### on Debian/Ubuntu
```
sudo apt-get install libfftw3-dev libtiff5-dev
```

### on Fedora
**Don't use Conda!**
```
dnf install fftw-devel
```

# Getting ready

First we load the `gplots` and `TargetDecoyMineR` packages.

We use `gplots` to visualize an example matrix and then we use `TargetDecoyMineR` to process it. 

```{r}
library(gplots)
library(TargetDecoyMineR)
```


# Example matrix

Here we set the number of samples and the number o variables. Also, we set the number of samples and the number of variables to perturb.

```{r}
m <- 20 	# number of samples
n <- 400	# number of variables (metabolites)
mPert <- m/2	# number of samples to perturb
nPert <- n/8	# number of variables to perturb
```

Using the parameters we already setup, we generate 4 matrix chunks.

```{r}
sdlog <- 0.5
# Generate x (data) chunks
set.seed(428)
x1 <- matrix(rlnorm(mPert*nPert, 0, sdlog), nrow = mPert, ncol = nPert, byrow = TRUE)
set.seed(76)
pert <- 2
x2 <- matrix(rlnorm(mPert*nPert, pert, sdlog), nrow = mPert, ncol = nPert, byrow = TRUE)
set.seed(8324)
x3 <- matrix(rlnorm(mPert*nPert, 0, sdlog), nrow = mPert, ncol = n-nPert, byrow = TRUE)
set.seed(83)
x4 <- matrix(rlnorm(mPert*nPert, 0, sdlog), nrow = mPert, ncol = n-nPert, byrow = TRUE)
```

We gather up the 4 matrix chunks to obtain our example matrix to be processed with `TargetDecoyMineR`.

```{r}
# Gather x chunks
x <- cbind(rbind(x1, x2), rbind(x2, x1), rbind(x3, x4))
dim(x)
x[1:4, 1:4]
```


## Visualize matrix to process

We visualize the example matrix using the `heatmap.2` function from the `gplots` package.

```{r}
# Plot x 
cols <- colorRampPalette(c("purple", "black", "yellow"))(40)
heatmap.2(as.matrix(log10(x+0)), trace = "n", 
          labCol = FALSE,
          # labRow = rep(c("Control", "Treatment"), c(5, 5)),
          labRow = FALSE,
          sepwidth = c(0.5, 0.001), sepcolor = c("white"), rowsep = seq(m), colsep = nPert*2, 
          key = TRUE, key.xlab = "log10(x+1)", key.title = NA,
          col = cols, Rowv = FALSE, Colv = FALSE, dendrogram = "n", 
          RowSideColors = rep(c("lightsteelblue1", "lightsalmon"), c(m/2, m/2)),
          ColSideColors = rep(c("gold2", "gray25"), c(nPert*2, ncol(x)-nPert*2)))
```


## Mean for each variable per class

Also, we calculate the sample mean per variable to observe the effect of the perturbation.

```{r}
# Variable means
means <- apply(log(x), 2, function(x, s) c(mean(x[s]), mean(x[!s])), seq(m) <= m/2)
matplot(t(means), type = "l", lty = 1, col = c("lightsalmon", "lightsteelblue1"), lwd = 3, 
        yaxt = "n", ylim = range(means),
        # ylab = "log(mean)",
        frame.plot = F, ylab = "")
axis(side = 2, at = range(means), labels = c("0", "Perturbation"))

# Setup sample table to generate decoys
inputTab_tdm <- data.frame(Sample =  factor(rep(c("Cl", "Tr"), c(m/2, m/2))), x, stringsAsFactors = FALSE)

# Set formula to Target-Decoy MineR
formula <- as.formula(paste0("Sample ~ ", paste(colnames(inputTab_tdm)[grepl("X", colnames(inputTab_tdm))], 
                                                collapse = " + ")))
```


# Target-Decoy MineR

We use the `TDMineR` function from the `TargetDecoyMineR` package to analyze the example matrix.

You must provide a data frame to TargetDecoy MineR with a column named "Sample" and columns with all the variables you want to use in the process.

If you have a column Sample named differently, e.g. Class, then you have to set the formula as "Class ~ .".

```{r}
# Target-Decoy MineR
tdm <- TDMineR(formula = "Sample ~ .", data = inputTab_tdm, nFolds = 5, scale. = TRUE, method0 = "tdm")
```


## Explore Target-Decoy MineR results

Now, we `print` the resulting `TDMineR` object,

```{r}
# Print tdm
print(tdm)
```

its summary,

```{r}
# Summary tdm
summary(tdm)
```

and we plot it. In this `TargetDecoyMineR` summary plot, we can identify how the relevant variables help to separate the 2 type of samples, Control and Treatment.

```{r}
# Plot target decoy MineR results
plot(tdm)
```

Finally, we get what are the relevant variables by accessing the ids element of the `tdm` object.

```{r}
# Get relevant variables
tdm$ids
```


```{r}
sessionInfo()
```


